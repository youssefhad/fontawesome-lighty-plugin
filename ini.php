<?php 

namespace Lighty\Plugins;
use Lighty\Kernel\Foundation\Application;

/**
* Mother Class of the plugin
*/
class FontAwesome
{
	
	/**
	* to set script to call the css file
	*/		
	public static function set()
	{
		echo '<link rel="stylesheet" type="text/css" href="'.Application::$root.'plugins/fontawesome-lighty-plugin/core/css/font-awesome.min.css">';
	}
}